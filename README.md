# Change Advisory Board



* Project for the team members of the CAB.   No access to users outside of CAB membership.

* New requests for CAB approval should be submitted here: https://gitlab.com/kkwentus1-demo/planning-and-approvals/cab-approvals/-/issues
